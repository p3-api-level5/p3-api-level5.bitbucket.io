/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 72.24755700325733, "KoPercent": 27.752442996742673};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.08534201954397394, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.005128205128205128, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.0, 500, 1500, "getCountClassAttendance"], "isController": false}, {"data": [0.0, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.6522842639593909, 500, 1500, "getBroadcasting"], "isController": false}, {"data": [0.0024875621890547263, 500, 1500, "me"], "isController": false}, {"data": [0.002777777777777778, 500, 1500, "getClassAttendance"], "isController": false}, {"data": [0.002857142857142857, 500, 1500, "saveBroadcasting"], "isController": false}, {"data": [0.0, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 1535, 426, 27.752442996742673, 362395.2143322473, 5, 610156, 406972.0, 600102.0, 606130.2, 609730.88, 1.9584827181443536, 2.8624057739355933, 5.2410120913979865], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 195, 21, 10.76923076923077, 312598.7487179485, 1200, 609527, 327369.0, 600003.0, 600090.8, 609264.92, 0.2853601464556075, 0.17624504598322668, 0.35307744683520964], "isController": false}, {"data": ["getCountClassAttendance", 206, 45, 21.844660194174757, 385094.4805825241, 45607, 609844, 399074.5, 600078.6, 600116.3, 609416.28, 0.27920580856511257, 0.15989503987831505, 0.3119252392563367], "isController": false}, {"data": ["getHomefeed", 192, 140, 72.91666666666667, 572256.9270833329, 313660, 610050, 600062.0, 608832.4, 609467.05, 609997.92, 0.25357916424533783, 1.8775870213198005, 1.275820170109356], "isController": false}, {"data": ["getBroadcasting", 197, 0, 0.0, 3839.4619289340085, 5, 37988, 99.0, 10172.0, 21389.399999999983, 33736.760000000046, 0.6307330270382762, 0.41083879788528344, 1.4363959170441993], "isController": false}, {"data": ["me", 201, 89, 44.27860696517413, 482685.6766169155, 1179, 610047, 584699.0, 606305.2, 608841.2999999999, 610018.64, 0.28876903936166237, 0.2343007537626175, 0.915093293680268], "isController": false}, {"data": ["getClassAttendance", 180, 33, 18.333333333333332, 357964.6333333333, 1229, 610030, 372548.5, 600068.0, 600109.85, 609988.69, 0.25588063452711124, 0.12965618349768926, 0.5614880720531435], "isController": false}, {"data": ["saveBroadcasting", 175, 31, 17.714285714285715, 324869.6057142856, 1259, 609773, 311959.0, 600055.8, 600086.4, 609540.4400000001, 0.25478819821065884, 0.15059944610865625, 0.4579277575362709], "isController": false}, {"data": ["getChildCheckInCheckOut", 189, 67, 35.44973544973545, 460608.96825396805, 143558, 610156, 522927.0, 600094.0, 603113.0, 609715.0, 0.2411421718106077, 0.12423800788177654, 1.1166954870370134], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["502/Bad Gateway", 425, 99.76525821596245, 27.68729641693811], "isController": false}, {"data": ["Non HTTP response code: javax.net.ssl.SSLHandshakeException/Non HTTP response message: Remote host terminated the handshake", 1, 0.2347417840375587, 0.06514657980456026], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 1535, 426, "502/Bad Gateway", 425, "Non HTTP response code: javax.net.ssl.SSLHandshakeException/Non HTTP response message: Remote host terminated the handshake", 1, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["getCountCheckInOutChildren", 195, 21, "502/Bad Gateway", 21, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getCountClassAttendance", 206, 45, "502/Bad Gateway", 45, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getHomefeed", 192, 140, "502/Bad Gateway", 140, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["me", 201, 89, "502/Bad Gateway", 89, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["getClassAttendance", 180, 33, "502/Bad Gateway", 33, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["saveBroadcasting", 175, 31, "502/Bad Gateway", 30, "Non HTTP response code: javax.net.ssl.SSLHandshakeException/Non HTTP response message: Remote host terminated the handshake", 1, null, null, null, null, null, null], "isController": false}, {"data": ["getChildCheckInCheckOut", 189, 67, "502/Bad Gateway", 67, null, null, null, null, null, null, null, null], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
